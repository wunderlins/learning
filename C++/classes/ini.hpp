/**
 * Simple ini file parser.
 *
 * Usage: `Ini::Ini ini(argv[1]);` where argv[1] is a string file name or 
 *        an ifstream.
 */
namespace Ini {

#ifdef __MINGW32__
	const std::string nl = "\r\n";
#else
	const std::string nl = "\n";
#endif

/**
 * holds an ini section
 *
 * a section in an ini file is defined by a line start starts with '[', 
 * followed by a string which is the identifier of the section, ending in ']'.
 * 
 * after the section name, key/value pairs follow on each subsequent line 
 * (\r\n, \r or \n delimited). The key is delimited from the value by '=' 
 * (ignore any whitespace before and after the equal sign).
 */
class Section {

public:
	// key value storage
	std::map<std::string, std::string> kv;
	
	// the section's name
	std::string name;
	
	// Constructors
	Section() {this->name = "";};
	Section(const std::string name) { this->name = name;};
	
	// destructor
	~Section() {
		//delete kv;
		//delete name;
		kv.clear();
	};

	// get number of elements in map
	int length() { return kv.size(); };
	
	// add a key value pair
	void add_kv(const std::string& k, const std::string& v);
};

/**
 * This class holds the representation of an ini file
 *
 * an ini file consists of several sections (if not empty or malformed). 
 * Sections contain name/value pairs. 
 *
 * Sections are stored in `sections`, `sections` holds a public map `kv` which 
 * holds all key value pairs.
 *
 * Parse an ini file with `Ini::Ini ini(argv[1]);`.
 *
 * get a string representation with `ini.tostring();`.
 */
class Ini {
private:
	// 1-N sections. holds the name and 0-1 key/value pairs inside the section
	std::map<std::string, Section> sections;
	
	// iterator used for looping over sections
	std::map<std::string, Section>::iterator lastp;
	
	// reference for the parser to the latest section
	Section* current = nullptr;
	
	// string trim functions (they probably belong into a string library)
	std::string& ltrim(std::string& str, const std::string& chars = "\t\n\v\f\r ");
	std::string& rtrim(std::string& str, const std::string& chars = "\t\n\v\f\r ");
	std::string& trim(std::string& str, const std::string& chars = "\t\n\v\f\r ");
	
public:
	// add a section to our section store
	Section* add_section(std::string name);
	
	// parse an ini file line by line
	void parse_file(std::ifstream& fsini);
	
	// constructors
	Ini(std::ifstream& fsini) {
		parse_file(fsini);
	};
	
	Ini(std::string filename) {
		//open the file
		std::ifstream fsini(filename);
		// parse it
		parse_file(fsini);
		// free the file ressources
		fsini.close();
	};
	
	// destructor
	~Ini() {
		sections.clear();
		//delete sections;
		//delete current;
		//delete lastp;
	};
	
	// singleton
	Ini parse(std::string inifile);
	
	// convert data structure to string representation
	std::string tostring();
};

} // end namespace ini