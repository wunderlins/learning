#include <iostream> // std in/out/err
#include <fstream> // files
#include <string> // for string class
#include <map> // for map class

using namespace std;

namespace str {
#define STR_DELIMS "\t\n\v\f\r "
string& ltrim(string& str, const string& chars = STR_DELIMS) {
	str.erase(0, str.find_first_not_of(chars));
	return str;
}

string& rtrim(string& str, const string& chars = STR_DELIMS) {
	str.erase(str.find_last_not_of(chars) + 1);
	return str;
}

string& trim(string& str, const string& chars = STR_DELIMS) {
	return ltrim(rtrim(str, chars), chars);
}
} // end namespace str

namespace Ini {
enum Type {NONE, BLANK, EMPTY, COMMENT, SECTION, KEYVALUE};

typedef struct {
	string& line;
	Type type;
	size_t begin;
	size_t equal;
} LineType;

string empty = "";

class Line {
protected:
	int lineno = -1;
	LineType linetype {empty, NONE, string::npos, string::npos};
	virtual void parse_line(string& ln);

public:
	Line(int lno, string ln) {
		lineno = lno;
		parse_line(ln);
	}
	
	static LineType get_type(string& ln) {
		auto l = ln.length();
		
		// blank and empty (white-space) lines
		if (l == 0) { 
			LineType r = {ln, BLANK, 0, string::npos}; 
			return r; 
		}
		
		auto begin = ln.find_first_not_of(STR_DELIMS);
		if (l <= begin) { 
			LineType r = {ln, EMPTY, begin, string::npos};
			return r;
		}
		
		// line comments
		if (ln.at(begin) == ';') {
			LineType r = {ln, COMMENT, begin, string::npos};
			return r;
		}
		
		// sections
		if (ln.at(begin) == '[') {
			LineType r = {ln, SECTION, begin, string::npos};
			return r;
		}
		
		// key value pairs
		auto equal = ln.find_first_of("=", begin);
		if (equal != string::npos) {
			LineType r = {ln, KEYVALUE, begin, equal};
			return r;
		}
		
		// unknown line type
		LineType r = {ln, NONE, string::npos, string::npos};
		return r;
	}
};

class KV_line: public Line {
protected:
	/** parse line
	 *
	 * find out if there is anything of use. categorize the 
	 * line and split it into it's relevant parts.
	 */
	void parse_line(string& ln) {
		// extract comment
		// split n/v pairs
		// store local line
		;
	}
	
	
public:
	string line;
	string name;
	string value;
	string comment;
	
	KV_line (int lno, string ln): Line(lno, ln) {};
	
	string tostring() {
		return line;
	}
	
};

class Ini {
	int n = 0;
	map<int, Line> lines;

	void parse_file(string filename) {
		//open the file
		ifstream fsini(filename);
		// parse it
		parse_io(fsini);
		// free the file ressources
		fsini.close();
	}
	
	void parse_io(ifstream& io) {
		//cout << "Parsing io" << endl;
		string line = "";
		while (getline(io, line)) {
			n++;
			//KV_line l(n, line);
			
			LineType lt = Line::get_type(line);
			//Type lt = NONE;
			cout << "type: " << lt.type << ": " << lt.line << endl;
		}
	}
	
	void parse_line(string& line, string& name, string& value, string& comment);

public:
	Ini(string& inifile) {
		parse_file(inifile);
	}
	Ini(ifstream& io) {
		parse_io(io);
	}
	
	string tostring();
};

} // end namespace 

#ifdef WITH_MAIN
void usage() {
	cout << "Usage: ini <infile> [outfile]" << flush << endl;
}

int main(int argc, char* argv[], char** envp) {
	if (argc < 2 || argc > 3) {
		cerr << "Invalid number of arguments" << endl << flush;
		usage();
		return 1;
	}
	
	string inifile = argv[1];
	
	/*
	// test iostream 
	ifstream fsini(inifile);
	// parse it
	Ini::Ini2 ini1(fsini);
	// free the file ressources
	fsini.close();
	*/
	
	// test filename
	Ini::Ini ini(inifile);
	
	return 0;
}
#endif