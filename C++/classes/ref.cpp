#include <iostream>
#include <string>
#include <memory>

using namespace std;

class A {
	int id = -1;
	
public:
	static int count;
	string name;
	
	int get_id() {return id;};
	
	A() {};
	A(string& n) {
		//cout << "count: " << count << endl;
		increment(n);
		name = n;
	};
	
	void increment(string& n) {
		n = n.replace(n.end()-1, n.end(), to_string(count));
		id = count++;
	};
	
	string tostring() {
		return "Name: " + name + ", id: " + to_string(count);
	};
};
int A::count = 0;

int main() {
	const int l = 3;
	string name = "i am node 0";
	
	A arr[3];
	
	for(int i=0; i<l; i++) {
		arr[i] = A(name);
		cout << "Name: " << name << endl;
	}
	
	cout << "===================================" << endl;
	
	for(int i=0; i<l; i++) {
		cout << arr[i].tostring() << endl;
	}
	
	string n2 = "me memememe";
	A a1(n2);
	
	return 0;
}
