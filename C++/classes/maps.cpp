#include<iostream> // std in/out/err
#include <fstream> // files
#include<string> // for string class
#include<map> // for map class
using namespace std;

class Section { 
private:

public:
	map<string, string> kv;
	string name;
	Section(const string name) { this->name = name; };
	Section()                  { this->name = "";   };
	
	int length() {
		return kv.size();
	};
	
	void add_kv(const string k, const string v) {
		kv.insert(pair<string,string>(k, v) );
	};
};

int main() {
	
	map<string, Section> sections;
	sections.insert(pair<string, Section>("S1", Section("S1")) );
	//sections.insert(pair<string, string>("S2", "S2") );
	
	// get last element from 
	map<string, Section>::iterator lastp = --(sections.end());
	
	Section* last_section = &lastp->second;
	cout << last_section->name << endl;
	
	
	return 0;
}
