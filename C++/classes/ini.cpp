#include <iostream> // std in/out/err
#include <fstream> // files
#include <string> // for string class
#include <map> // for map class
#include "ini.hpp"

using namespace std;

namespace Ini {

void Section::add_kv(const string& k, const string& v) {
	
	// update ?
	map<string, string>::iterator it = kv.find(k); 
	if (it != kv.end()) {
		it->second = v;
		return;
	}
	
	// insert
	kv.insert(pair<string, string>(k, v) );
};

string& Ini::ltrim(string& str, const string& chars) {
	str.erase(0, str.find_first_not_of(chars));
	return str;
}

string& Ini::rtrim(string& str, const string& chars) {
	str.erase(str.find_last_not_of(chars) + 1);
	return str;
}

string& Ini::trim(string& str, const string& chars) {
	return ltrim(rtrim(str, chars), chars);
}

Section* Ini::add_section(string name) {
	auto ret = sections.insert(pair<string, Section>(name, Section(name)));
	if (ret.second==false)
		cout << "already exists: " << name << endl;
	lastp = --(sections.end()); // get last entry
	return &(lastp->second); // get ptr to last inserted section
};

void Ini::parse_file(ifstream& fsini) {
	string line, section {""};
	int lineno = 0;
	
	while (getline(fsini, line)) {
		++lineno; // increment line number
		string key, value, comment {""};
		
		// skip comments and empty lines
		if (line.length() == 0 || line.at(0) == ';' ||
			line.at(0) == '\r' || line.at(0) == '\n') continue;
		
		// remove comments, everything after ';'
		size_t semicolon = line.find(";");
		if (semicolon != string::npos) {
			// copy comment without semicolon
			comment = line.substr(semicolon+1);
			
			// remove comment from line buffer
			line = line.substr(0, semicolon);
		}
		
		// is this a section?
		trim(line);
		if (line.at(0) == '[') {
			// the last character must be ']' or this is not a section
			if (line.substr(line.length()-1) != "]") {
				cerr << lineno << ": Section not closed with ']', skipping" << endl;
				continue;
			}
			
			// extract section name
			section = line.substr(1, line.length()-2);
			current = add_section(section);
			//cout << "new section: " << current->name << endl;
			continue;
		}
		
		// we need to have a section to advance from here
		if (current == nullptr) {
			cerr << lineno << ": We don't have a section yet, skipping line: (" << lineno << "): " << line << endl;
			continue;
		}
		
		// split line by first occurrence of '='
		size_t equal = line.find("=");
		if (equal == string::npos) {
			cerr << lineno << ": Failed to find equal sign, skipping line: " << lineno << endl;
			continue;
		}
		key   = line.substr(0, equal);
		value = line.substr(equal+1);
		
		// clean up the key and value, remove leading and trailing whitespace
		trim(key);
		trim(value);
		
		// add pair to our section
		current->add_kv(key, value);
	}
};
string Ini::tostring() {
	// loop over all sections
	map<string, Section>::iterator its;
	bool first = true;
	string buffer = "";
	
	for (its = sections.begin(); its != sections.end(); its++) {
		map<string, string>::iterator itkv;
		
		// display section name
		if (first == true) { // no perpending newline for first section
			first = false;
		}	else {
			buffer += nl;
		}
		buffer += "[" + its->first + "]" + nl;
		
		// construct key=value line
		for (auto& x: its->second.kv) {
			buffer += x.first + " = " + x.second + nl;
		}
	}
	buffer += nl;
	
	return buffer;
};

Ini Ini::parse(string inifile) {
	Ini ini = Ini(inifile);
	return ini;
};

} // end namespace ini

#ifdef WITH_MAIN
void usage() {
	cout << "Usage: ini <infile> [outfile]" << flush << endl;
}

int main(int argc, char* argv[], char** envp) {
	if (argc < 2 || argc > 3) {
		cerr << "Invalid number of arguments" << endl << flush;
		usage();
		return 1;
	}
	
	// create ini object. This reads and parses the file content.
	Ini::Ini ini(argv[1]);
	
	// display volatile data of Ini
	string sini = ini.tostring();
	
	cout << sini << flush;
	return 0;
}
#endif