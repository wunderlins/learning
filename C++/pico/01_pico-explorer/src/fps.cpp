#include <string.h>
#include <math.h>
#include <vector>
#include <cstdlib>

#include "fps.hpp"
#include "pico_explorer.hpp"
#include "libraries/pico_graphics/pico_graphics.hpp"

// globals
static volatile unsigned long long seconds_elapsed = 0;
static volatile unsigned long long last_second = 0;
static volatile unsigned long long count = 0;
static volatile unsigned long long fps = 0;
static char fps_str[32] = {0};
static repeating_timer_t timers[10] = {0,};
static pimoroni::Point *position = nullptr;

static bool counter_cb(repeating_timer_t *rt) {
    unsigned long long *count = (unsigned long long*) rt->user_data;
    // update count by one second
    *count += 1;
    return true;
}

void fps_setup() {
    position = new pimoroni::Point(3, 3);
    bool res = add_repeating_timer_ms(1000,
        counter_cb,
        (void*) &seconds_elapsed,
        timers);
    
    if (res == false)
        exit(1);
}

void fps_loop(pimoroni::PicoGraphics_PenRGB332 *graphics, pimoroni::Pen *color)  {

    graphics->set_pen(*color);
    sprintf(fps_str,"FPS: %llu", fps);
    graphics->text(fps_str, *position, 180);

    if (last_second < seconds_elapsed) {
        // update screen
        last_second = seconds_elapsed;
        fps = count;
        count = 0;

    } else {
        count++;
    }
}
