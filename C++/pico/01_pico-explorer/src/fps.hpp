#include "libraries/pico_graphics/pico_graphics.hpp"

void fps_setup();
void fps_loop(pimoroni::PicoGraphics_PenRGB332 *graphics, pimoroni::Pen *color);