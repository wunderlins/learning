#include <string.h>
#include <math.h>
#include <vector>
#include <cstdlib>

#include "pico_explorer.hpp"
#include "drivers/st7789/st7789.hpp"
#include "libraries/pico_graphics/pico_graphics.hpp"
#include "fps.hpp"

// globals
pimoroni::ST7789 *display                  = nullptr;
pimoroni::PicoGraphics_PenRGB332 *graphics = nullptr;
pimoroni::Pen BG;    
pimoroni::Pen WHITE; 

void setup() {
    display = new pimoroni::ST7789(PicoExplorer::WIDTH, 
                 PicoExplorer::HEIGHT, ROTATE_0, 
                 false, 
                 get_spi_pins(BG_SPI_FRONT));
    graphics = new pimoroni::PicoGraphics_PenRGB332(display->width, display->height, nullptr);
    graphics->set_font(&font8);

    BG    = graphics->create_pen(120, 40, 60);
    WHITE = graphics->create_pen(255, 255, 255);

    fps_setup();
}

void loop()  {
    // reset sceen
    graphics->set_pen(BG);
    graphics->clear();

    // render fps counter
    fps_loop(graphics, &WHITE);

    // update screen
    display->update(graphics);

}

int main() {
    setup();

    while(true) {
        loop();
    }
    
}