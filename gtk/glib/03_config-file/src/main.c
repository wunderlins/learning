#include <glib.h>
#include <glib/gprintf.h>

int main (int argc, char **argv) {
    g_autoptr(GError) error = NULL;
    g_autoptr(GKeyFile) key_file = g_key_file_new ();
    GKeyFileFlags flags = G_KEY_FILE_NONE;

    if (!g_key_file_load_from_file (key_file, "config.ini", flags, &error)) {
        if (!g_error_matches (error, G_FILE_ERROR, G_FILE_ERROR_NOENT))
        g_warning ("Error loading key file: %s", error->message);
        return 1;
    }

    g_autofree gchar *val = g_key_file_get_string (key_file, "First Group", "Name", &error);
    if (val == NULL &&
        !g_error_matches (error, G_KEY_FILE_ERROR, G_KEY_FILE_ERROR_KEY_NOT_FOUND)) {
        g_warning ("Error finding key in key file: %s", error->message);
        return 2;
    } else if (val == NULL) {
        // Fall back to a default value.
        val = g_strdup ("default-value");
    }

    g_printf("Name: %s\n", val);

    // iterate over all groups
               gsize   length = 0;
    g_autofree gchar **groups = g_key_file_get_groups(key_file, &length);
    g_printf("Number of Groups: %lu\n", length);

    for(gsize i = 0; i<length; i++) {
        g_autofree gchar *group = groups[i];
        g_printf("[%lu] '%s'\n", i, group);

        // now fetch all keys
        gsize num_keys = 0;
        g_autofree gchar **keys = g_key_file_get_keys (key_file, group, &num_keys, &error);
        if (error != NULL) {
            g_error("Failed to fetch keys of '%s': %s\n", group, error->message);
            return 3;
        }

        for (gsize ii=0; ii<num_keys; ii++) {
            g_autofree gchar *key = keys[ii];
            g_autofree gchar *val = g_key_file_get_string (key_file, group, key, &error);
            g_printf("\t[%lu] '%s': '%s'\n", ii, keys[ii], val);
        }
    }

    return 0;
}
