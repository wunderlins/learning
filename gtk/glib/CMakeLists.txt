cmake_minimum_required(VERSION 3.18)
set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_CXX_EXTENSIONS        ON)


project(Learning-GLib VERSION 1.0.0)

add_compile_options ( -Wall -g )

#include(FetchContent)
#SET(SRC_GLIB https://gitlab.gnome.org/GNOME/glib.git)
#SET(SRC_GLIB_VERSION 95115f029d9c170c2e966cd7d3547b6394c92a4a) # 2.66.7
#FetchContent_Declare(
#  glib
#  GIT_REPOSITORY "${SRC_GLIB}"
#  GIT_TAG        "${SRC_GLIB_VERSION}"
#)
#FetchContent_MakeAvailable(glib)

add_subdirectory("01_basics")
#add_subdirectory("02_gtk-test")
add_subdirectory("03_config-file")
