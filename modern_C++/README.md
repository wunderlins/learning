# Build


- generate: `cmake -S . -B build`
- clean: `cmake --build build --config Debug --target all --clean-first`
- build all targets: `cmake --build build --config Debug --target all`