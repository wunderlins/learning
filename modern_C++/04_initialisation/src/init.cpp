#include <iostream>
#include <memory>
#include <vector>
#include <string.h>
#include <stdlib.h>

void init_with_auto() {
	auto i = 42;          // int
	auto d = 42.5;        // double
	auto s = "text";      // char const *
	auto v = { 1, 2, 3 }; // std::initializer_list<int>
}

void init_with_auto_specific_type() {
	auto b  = new char[10]{ 0 };            // char*
	auto s1 = std::string {"text"};         // std::string
	auto v1 = std::vector<int> { 1, 2, 3 }; // std::vector<int>
	auto p  = std::make_shared<int>(42);    // std::shared_ptr<int>
}

/**
	* @brief To declare a function return type when you don't want to commit to a specific type:
	* 
	* @tparam F 
	* @tparam T 
	* @param f 
	* @param value 
	* @return auto 
	*/
template <typename F, typename T>
auto lambda_apply(F&& f, T&& value)
{
	return f(value);
}

void init_lambda() {
	/**
	 * @brief To declare named lambda functions, with the form 
	 * auto name = lambda-expression, unless the lambda needs to be passed 
	 * or returned to a function:
	 */
	auto upper = [](char const c) {return toupper(c); };
	
	/**
	 * @brief To declare lambda parameters and return values:
	 */
	auto add = [](auto const a, auto const b) {return a + b;};
	
	// use template
	auto ret = lambda_apply(atoi, "123");
	printf("ret is %d\n", ret); // int
	//char str[] = "abc";
	auto str = new char[4]{'a', 'b', 'c', 0};
	printf("ret is: %s\n", lambda_apply(strfry, str));
}

int main() {
	/**
	 * @brief To declare local variables with the `form auto name = expression`
	 * when you do not want to commit to a specific type:
	 */
	init_with_auto();
	
	/**
	 * @brief To declare local variables with 
	 * the `auto name = type-id { expression }` form when you need to commit 
	 * to a specific type: 
	 */
	init_with_auto_specific_type();
	
	/**
	 * @brief lambda delcarations
	 */
	init_lambda();
	
	return 0;
}