#include <iostream>
#include <fstream>
#include <string>
#include <memory>
#include <iostream>
#include <sstream>

#include "file.h"

using namespace std;

int main() {
    // find temp location
    auto tmp = make_unique<char*>();
    *tmp = getenv("TMP");
    if (*tmp == nullptr)
        *tmp = getenv("TEMP");
    if (*tmp == nullptr)
        *tmp = (char*) "/tmp";
    stringstream tmpfile;
    tmpfile << *tmp << "/outfile.txt";
    cout << "temp file name: " << tmpfile.str() << endl;

    // buffer some data into a string buffer
    auto buffer = make_unique<stringstream>();
    *buffer << "Line 1" << endl;
    *buffer << "line 2" << endl;

    cout << buffer->str();

    // write the string buffer to a file
    ofstream fs{ tmpfile.str() };
    fs << buffer->rdbuf(); // streams string to output file

    // append buffer and flush it to file
    *buffer << "line 3" << endl;
    fs << buffer->rdbuf(); 

    // close file stream
    fs.close();
}