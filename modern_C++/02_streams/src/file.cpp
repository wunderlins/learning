#include <iostream>
#include <fstream>
#include <string>
#include <memory>
#include <sstream>

#include "file.h"

using namespace std;

int main() {

    // find temp location
    auto tmp = make_unique<char*>();
    *tmp = getenv("TMP");
    if (*tmp == nullptr)
        *tmp = getenv("TEMP");
    if (*tmp == nullptr)
        *tmp = (char*) "/tmp";
    
    stringstream tmpfile;
    tmpfile << *tmp << "/tempfile.txt";
    cout << "Temp file location: " << tmpfile.str() << endl;
    
    // write some data to the file
    fstream fs{ tmpfile.str(), std::ios::out };
    fs << "i am a line" << endl << "and i am another line" << endl;
    fs.close();

    // output file content to stdout, char by char
    fstream fs1{ tmpfile.str() };
    char c;
    while (fs1 >> noskipws >> c) {
        cout << c;
    }

    // rewind read pointer
    fs1.clear();
    fs1.seekg(0);

    std::string s;
    while (fs1) {
        std::getline(fs1, s); // read each line into a string
        std::cout << s;
    }
    std::cout << '\n';
    
}
