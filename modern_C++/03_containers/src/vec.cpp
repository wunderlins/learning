#include <iostream>
#include <memory>
#include <vector>

using namespace std;

int main() {
    
    //declare vector
    auto v1 = make_unique<vector<int>>(vector<int>{1, 2, 3, 4, 5});
    auto v = *v1;
    std::cout << "The third element is:" << v[2] << '\n';
    std::cout << "The fourth element is:" << v.at(3) << '\n';

    // add an eleemnt
    v.push_back(10);

    // loop over all elements
    for (auto el : *v1) {
        cout << el << endl;
    }

    /*
    auto v2 = new vector<int>({1, 2, 3}); // leak, not freed
    int a[5];
    a[7] = 0; // stack overrun
    cout << a[7];
    */
}
