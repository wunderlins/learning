#pragma once

#include <string>
#include <sstream>
#include <memory>
#include <vector>
#include <map>

// fwd declarations
class NodeAbstract;
class Segment;
class FieldList;
class Field;
class Component;
class SubComponent;

class NodeAbstract {
public:
    std::vector<unsigned char> data{};
    std::size_t length() { return this->data.size(); }
    std::size_t num_children() { return this->children.size(); }
    std::vector<NodeAbstract*> children{};

    ~NodeAbstract() {
        ;
    }
    void addChild(NodeAbstract* child) { this->children.push_back(child); }
    void addChild(std::string data) {};
};

class SubComponent: public NodeAbstract {
public:
    void addChild(std::string data) {
        throw std::range_error("SubComponent cannot have children");
    }
};

class Component: public NodeAbstract {
public:
    std::vector<SubComponent*> children{};
};

class Field: public NodeAbstract {
public:
    std::vector<Component> children{};
};

class FieldList: public NodeAbstract {
public:
    FieldList(std::string data = ""):
    NodeAbstract() {
        this->data.insert(this->data.begin(), data.begin(), data.end());
    }
    std::vector<Field*> children{};
};

class Segment: public NodeAbstract {
public:
    Segment(std::string name):
    NodeAbstract() {
        this->data.insert(this->data.begin(), name.begin(), name.end());
    }
    std::vector<FieldList*> children{};
    void addChild(std::string data) {
        auto fl = new FieldList{data};
        this->children.push_back(fl);
    }
};

class Document {
    public:
        std::map<std::string, Segment> data = {};
        void append(Segment seg) {
            //auto k = std::string(reinterpret_cast<char*>(seg.data.data()));
            //data.insert(k, seg);
        }
};
