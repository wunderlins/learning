#include <iostream>
#include <string>
#include <memory>
#include <sstream>

#include "node.h"

using namespace std;

int main() {
    auto s1 = *(make_unique<Segment>("MSH"));
    cout << "s1.length: " << s1.length() << endl;
}