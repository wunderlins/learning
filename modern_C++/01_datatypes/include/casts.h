#pragma once

class base {
public:
    virtual const char* hello();
};

class ext1 : public base {
public:
    const char* hello();
};