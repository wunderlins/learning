#include <iostream>
#include <fstream>
#include "datatypes.h"
#include "version.h"

using namespace std;

int main( int argc, char* argv[] ) {
	// report version
	std::cout << argv[0] << " Version " << Datatypes_VERSION_MAJOR << "."
						<< Datatypes_VERSION_MINOR << "." << Datatypes_VERSION_PATCH << std::endl;
}