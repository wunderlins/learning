#include <iostream>
#include <fstream>
#include <memory>

#include "casts.h"
#include "version.h"

using namespace std;

const char* base::hello() {
    return"Hello from base";
}

const char* ext1::hello() {
    return "Hello from ext";
}

void ptr(void) {
    auto p = make_unique<int>(123);
    cout << "p: " << *p << endl;
}

int main( int argc, char* argv[] ) {
	// report version
	cout << argv[0] << " Version " << Datatypes_VERSION_MAJOR << "."
					<< Datatypes_VERSION_MINOR << "." << Datatypes_VERSION_PATCH << endl;

    auto i = static_cast<int>(123);
    auto b0 = static_cast<bool>(0);
    auto b1 = static_cast<bool>(i);
    cout << "int: " << i << ", bool (false): " << b0 << ", bool (true): " << b1 << endl;

    ptr();

    // create smart pointer
    auto e = make_unique<ext1>();
    cout << e->hello() << endl;
}